import hashlib 

def BruteDigits(code, debug=False):
    for i in range(100000): # creates grid * 100000
        i = str(i)

        if len(i) == 1:
            i = "000" + i
        elif len(i) == 2:
            i = '00' + i
        elif len(i) == 3:
            i = '0' + i
        
        i = 'SKY-BMYS-' + i
        plaintext = i
        i = i.encode('utf-8')
        i = str(hashlib.md5(i).hexdigest())

        if debug:
            print('i:      ' + i)
            print('code:   ' + code)

        if i == code:
            print('Found:  ' + i)
            print('Text:   ' + plaintext)

BruteDigits('d02467d7a483edf37a890b23b604e3e4')



targets = ['mnvan567','21calvin','TADL2008']

def TestHash(target):
    target = target.encode("UTF-8")

    print(f"{target} hash: ", hashlib.sha256(target).hexdigest())
"""
for i in range(len(targets)):
    TestHash(targets[i])
"""